import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quienes-somos',
  templateUrl: './quienes-somos.component.html',
  styleUrls: ['./quienes-somos.component.css']
})
export class QuienesSomosComponent implements OnInit {

  titulo = 'Quiénes somos';
  descripcion = 'Somos un grupo de artistas en Ecuador con múltiples habilidades. El Diseño 2D/3D, Motion Graphics y Desarrollo Web Avanzado nos ayuda a crear múltiples y satisfactorios contenidos multimedia.';


  constructor() { }

  ngOnInit(): void {
  }

}
