import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Para agregar la rutas de la página. Comment by: Carlos Icaza
import {Routes, RouterModule} from "@angular/router";

//páginas de la web- Commented by Carlos Icaza
import { InicioComponent } from './inicio/inicio.component';
import { EquipoComponent } from './equipo/equipo.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ColaboradoresComponent } from './colaboradores/colaboradores.component';
import { QuienesSomosComponent } from './quienes-somos/quienes-somos.component';
import { PieDePaginaComponent } from './pie-de-pagina/pie-de-pagina.component';
import { ListaServiciosComponent } from './lista-servicios/lista-servicios.component';

//instanciamos una lista de rutas
const appRoutes: Routes =[
  {path: '', component: InicioComponent},
  {path: 'equipo', component: EquipoComponent},
  {path: 'servicios', component: ServiciosComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    EquipoComponent,
    ServiciosComponent,
    ColaboradoresComponent,
    QuienesSomosComponent,
    PieDePaginaComponent,
    ListaServiciosComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    //Agregamos las rutas. Comment by: Carlos Icaza
    RouterModule.forRoot(appRoutes)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
