import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie-de-pagina',
  templateUrl: './pie-de-pagina.component.html',
  styleUrls: ['./pie-de-pagina.component.css']
})
export class PieDePaginaComponent implements OnInit {

  CopyrightStr="© 2020 Copyright:";
  CopyrightStr2="Modream";
  Cta_Str="Regístrate gratis";
  Cta_Str2=" Suscríbete";

  titleFooter="Contáctanos";
  textFooter="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste atque ea quis molestias. Fugiat pariatur maxime quis culpa corporis vitae repudiandae aliquam voluptatem veniam, est atque cumque eum delectus sint!";


  constructor() { }

  ngOnInit(): void {
  }

}
