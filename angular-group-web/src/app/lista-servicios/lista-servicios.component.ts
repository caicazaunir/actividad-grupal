import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-servicios',
  templateUrl: './lista-servicios.component.html',
  styleUrls: ['./lista-servicios.component.css']
})
export class ListaServiciosComponent implements OnInit {


  listServ = [
    {"Servicio":"Web Responsiva",},
    {"Servicio":"App móvil",},
    {"Servicio":"Animación 3D",},
    {"Servicio":"Animación 2D",},
    {"Servicio":"Ilustración",},
    {"Servicio":"Fotografía",},
    {"Servicio":"Impresión 3D",},
    {"Servicio":"Teaser videogame",},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
